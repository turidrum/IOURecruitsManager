# IOU Recruits Manager Documentation




## Table of contents

1. [Requirements](#requirements)
1. [Install](#install)
1. [Configuration](#configuration)
1. [Template keywords](#template-keywords)
1. [Usage](#usage)



## Requirements

Python 3 and IOUExportParser.

---

## Install

Download all the files in the repository with the same structure of directories.

Once you have downloaded `IOURecruitsManager.py`, you have to give it execution permission, on *nix systems you can do this:

```
chmod 755 IOURecruitsManager.py
```

Make a new directory named `templates` and create 3 new files in it, named `iou.template`, `kong.template` and `title.template`, like this:

```
mkdir templates
touch templates/iou.template templates/kong.template templates/title.template
```

---

## Configuration

The program rely on a easy to edit JSON config file like this:

```json
{
    "family_name": "Holy Warriors Family",
    "family_initials": "HWF",
    "guilds": [
	[
	    {
		"guild_name": "Holy Warriors",
		"guild_initials": "HW",
		"guild_rank": 25,
		"rank_target": 8,
		"entry_level": "300,000,000+ FP",
		"color": "ffff00",
		"import": {"members":[["brutalboy","Creator",1808,362436,218616302402],["bbtoskar","Leader",1761,342899,87000000000],["anonymoous","Member",1896,379296,231139000000],["xDx","Member",1798,327104,57955891993],["Modeon","Member",1682,323806,32817146597],["Aeric","Member",1670,326481,35178696893],["Dailen","Member",1669,309527,270560282971],["Darina","Member",1664,325508,158338303052],["Pobnutkin","Member",1659,327713,12269659260],["monoxxide","Member",1654,318400,9140350449],["Timonius","Member",1653,320424,59414236066],["dmwro","Member",1641,321975,111237322526],["bmerich33","Member",1641,313951,53287413361],["gooshy","Member",1618,316267,235828534797],["Gwondonolas","Member",1614,314660,214434049159],["Jelle","Member",1609,302530,80543270015],["Simigabi","Member",1591,309240,2482206407],["Clarisse62","Member",1573,305192,59283525572],["FigoWigo","Member",1572,287293,125380043123],["CharlieBMF","Member",1567,302542,32558571785],["Baga","Member",1533,299433,30357272873],["AkuneAntok","Member",1497,284766,118269974946],["Kelzan","Member",1495,286633,49527283068],["Flynt","Member",1458,277847,55667040605],["Rdbj","Member",1454,280488,103239753055],["mummoo","Member",1441,272519,123354908247],["Doica","Member",1435,272711,4644907843],["Aerow","Member",1385,262172,26397299991],["Huitzilin","Member",1361,233962,7137360611],["SuperDavid","Member",1360,259910,170259876277],["Crimson599","Member",1319,248249,29643303952],["raival","Member",1299,250013,61233792525],["ant292","Member",1293,247574,82517911077],["SARASAY","Member",1278,235401,4806009435],["locker01","Member",1271,240044,47215667341],["Cairo","Member",1264,230283,11634882125],["daninin","Member",1205,214248,4811522434],["Vampyre","Member",1062,192239,19436607725]],"buildings":[["Altar",130,3566537203,99215047797],["Fortress",124,994306273,82207118727],["Warehouse",140,546260228,141678589772],["Sacrificial Tower",221,2801981017,1281415018983],["Bank",102,418481711,30774703289],["Sawmill",130,3064211147,99717373853],["Library",120,2713243707,68590451293],["Aquatic Research",214,11805519117,1085293105883],["Stables",120,2835764719,68467930281],["Space Academy",124,2940584320,80260840680]],"guildLevel":201,"goldLevel":12.3}
	    }
	],
	[
	    {
		"guild_name": "NoMatterTheCost",
		"guild_initials": "NMTC",
		"guild_rank": 34,
		"rank_target": 25,
		"entry_level": "3,000,000+ FP",
		"color": "00ff00",
		"import": {"members":[["turidrum","Creator",1299,254782,85126413528],["Whalor","Leader",1444,265878,21154164288],["MrTom","Leader",1065,188345,11467922171],["OmegaMan5000","Member",1668,314921,204226165],["Faaaaaart","Member",1569,288271,1074354341],["Damador","Member",1472,280221,2335165676],["Trepotri","Member",1457,264378,418587875],["Lyve","Member",1424,262736,506889715],["JuicySamurai","Member",1390,250601,353663382],["Scattlove","Member",1357,254656,398464522],["rety","Member",1327,246290,915739066],["BlessingStar","Member",1307,237202,929678445],["Dawzs","Member",1303,232907,123918272],["cxvxx","Member",1282,233869,484983154],["heatseeker","Member",1271,221580,943234917],["Wildcolt","Member",1270,231127,4011204516],["vaar","Member",1249,225673,406837622],["Kamui1337","Member",1243,207419,277644144],["Mc0wn","Member",1238,219320,871888745],["Tidlz","Member",1228,217937,156968830],["Fruit","Member",1197,177027,140175000],["Xerox","Member",1191,196652,236178291],["IIIIIIIIIIIII","Member",1176,198742,150746051],["Rinnegan","Member",1169,208547,1894254311],["ofu","Member",1157,208658,768600960],["SnowWhite","Member",1149,189624,400390825],["Iyansa","Member",1123,185256,117637209],["Acmano179","Member",1083,128144,393739802],["MegaxAndRed","Member",1081,186790,125184061],["Gotrek","Member",1070,154073,326264706],["Iforgiveyou","Member",1006,175104,1702957386],["Jumpz","Member",986,172720,702645262],["Khrawn","Member",846,126009,179741600],["sunny123","Member",791,134569,1005680860],["OldBarbon","Member",782,130343,613008792],["Artiface","Member",776,127019,189071657],["trickier","Member",776,127019,190473921],["Guile","Member",766,124245,158084155],["Iluya","Member",707,118403,276585630],["DanyX94","Member",549,39391,205920256]],"buildings":[["Altar",100,1681037294,26045002706],["Fortress",70,393172265,5771032735],["Warehouse",97,430636636,23632138364],["Sacrificial Tower",80,585149375,10333065625],["Bank",70,394731867,5769473133],["Sawmill",98,717983146,24324891854],["Library",88,635399836,15549975164],["Aquatic Research",86,477732870,14297792130],["Stables",80,578517746,10339697254],["Space Academy",70,287983856,5876221144]],"guildLevel":118,"goldLevel":7.8}
	    }
	],
	[
	    {
		"guild_name": "The Necromancy",
		"guild_initials": "TN",
		"guild_rank": 54,
		"rank_target": 50,
		"entry_level": "50,000+ FP",
		"color": "00ffff",
		"import": {"members":[["FrancisSavior","Creator",1297,255067,78411488999],["Ptal","Member",972,167304,25514372],["Xfxforce","Member",912,152483,45464206],["Darkon","Member",811,137291,61795758],["Unquenchable","Member",791,120623,34384309],["Erasure","Member",779,130817,20834743],["aperikub","Member",778,131091,165377429],["deciever","Member",758,123646,134318715],["craftier","Member",757,123476,133049662],["artister","Member",751,122303,132579747],["subterfuge","Member",751,121485,135080230],["slyer","Member",751,122333,134424145],["Slicker","Member",747,121384,136106820],["artful","Member",747,121414,135201510],["duplicitous","Member",747,121530,133705495],["subtler","Member",747,121430,132268676],["PirionXII","Member",670,95234,37047427],["Lucretius","Member",662,78069,82273074],["Spaceframe","Member",658,104683,63355142],["SefcaToo","Member",648,68230,55018055],["Xincredible","Member",627,98494,29072670],["Tiopa","Member",573,67870,19670715],["Michele1","Member",564,57150,7511671],["ArchaDust","Member",555,91895,19542096],["Nakratash","Member",554,91352,16936266],["Halith","Member",529,32889,3943844],["WalrusRajere","Member",526,90912,32822058],["Reps","Member",522,60238,158715669],["bwar","Member",520,27389,1680731],["simbu","Member",509,29952,1287000],["renlok","Member",500,54607,40002210],["Krovax","Member",495,71541,4925943],["Joep","Member",460,55390,2605218],["BonnieB00","Member",448,34043,2266564],["Overon","Member",446,46233,14326205],["snoopyhead","Member",441,48004,7713443],["QContinuum","Member",406,39598,6342544],["aethnight","Member",403,44426,3811962],["Elektro","Member",375,25744,2358251]],"buildings":[["Altar",64,279113888,3862711112],["Fortress",60,253738109,2758706891],["Warehouse",100,1611604223,26114435777],["Sacrificial Tower",60,259349263,2753095737],["Bank",40,22301676,398973324],["Sawmill",100,1490825345,26235214655],["Library",60,258197595,2754247405],["Aquatic Research",60,252435161,2760009839],["Stables",82,127839946,12015545054],["Space Academy",50,106696710,948238290]],"guildLevel":98,"goldLevel":6.5}
	    }
	]
    ],
    "recruiting_threads": {
	"iou": "https://iourpg.com/forum/showthread.php?tid=1206",
	"kong": "http://www.kongregate.com/forums/12180-guild-recruitment/topics/543389"
    }
}
```

The outer object holds data about the coalition of guilds you are going to manage, it can have these keys:

- `family_name` is the name for your coalition.
- `family_initials` coalition initials or tag.
- `guilds` holds objects describing the guilds in this coalition.
- `recruiting_threads` holds links to your recruiting thread on iou and kongregate forums.

The `guilds` can hold one or more objects representing the guilds in your coalition, and each guild object can have these keys:

- `guild_name` is the name of the guild.
- `guild_initials` initials or tag of the guild.
- `guild_rank` a number representing the rank of the guild as found in game by clicking on Social -> Top Ranks -> Guilds.
- `rank_target` the rank position that this guild has as goal.
- `entry_level` a string representing the minimum requirements to enter this guild.
- `color` hex color representing this guild in the ranking chart.
- `import` data obtained from IOUExportParser.

Save this file as `config.json` and put it in the same directory where you have the file `IOURecruitsManager.py`.

Write your recruiting annunce in the template files, here you can also use special words explained later.

Example `iou.template`(iou's forum use mybb code):

```
[align=center]
[img]http://s3.postimg.org/t4csjad9v/holy_warriors.jpg[/img]
[/align]
[size=large][size=xx-large][b]{GUILD}[/b] are searching for active players[/size]
[/size]
If you are eligible, write here your in-game name, level and iou score.
[url="http://linktosomethinguse.ful"]i'm a link[/url]
```

Example `kong.template`(kong supports some markdown):

```markdown
![](http://s3.postimg.org/t4csjad9v/holy_warriors.jpg)
# **{GUILDNAME}**

If you are eligible, write here your in-game name, level and iou score.
[i'm a link](http://linktosomethinguse.ful)
```

These were too simple and single guild, look at two advanced multiguild examples of `iou.template` and `kong.template` templates:

```
[align=center][img]http://s22.postimg.cc/i2cff57fl/hwfamily_logo_black.png[/img][/align]
[align=center][size=xx-large][b]{HWF-NAME}[/b][/size][/align]
We offer to any member of our Family a complete and working platform that help you in various aspects of the game.
The platform is the Family's organization, a complex system which includes roles, rules, parties, communication, coordination, democracy, collaboration and so on.
If you play 24/7 we can grant you a permanent party FOREVER with similar DPS, we take care of you and your party mates making your party always working.

A little introduction to Holy Warriors Family can be found [url=http://pastebin.com/raw.php?i=7XF82u4g]here[/url].

If you want join us, [url=https://iourpg.com/forum/private.php?action=send&uid=829]send me a PM[/url].
We use Family Points(FP) to allocate players in our guilds, you can't know how they work until you join us.

{HWF-RANKINGSCHART}

[hr]


[align=center][img]http://s24.postimg.cc/rfftet4ed/HW_logo_forum.png[/img][/align]
[size=large][b]{HW-GUILD}[/b] - {HW-DYNAMICSLOTS} - Entry level {HW-ENTRYLEVEL}[/size]
We are ranked {HW-POSITION}, with your support we can easily break on top {HW-RANKTARGET}!
We are looking for active players who will use our forum and update their progress on our spreadsheet at least once a week(it requires ~5 minutes), these things allow us to organize in-Family (semi)permanent parties DPS based.

{HW-MEMBERSCOUNTSTRING}
{HW-LEVELFF}
{HW-WISHINGWELLFF}
{HW-IOUSCORE}
{HW-AVERAGELEVEL}
{HW-BUILDINGSLIST-TEXT}

[hr]


[align=center][img]http://s4.postimg.cc/6gp80wmct/NMTC_logo_forum.png[/img][/align]
[size=large][b]{NMTC-GUILD}[/b] - {NMTC-DYNAMICSLOTS} - Entry level {NMTC-ENTRYLEVEL}[/size]
We are ranked {NMTC-POSITION}, with your support we can easily break on top {NMTC-RANKTARGET}!
We are looking for active players who will use our forum and update their progress on our spreadsheet at least once a week(it requires ~5 minutes), these things allow us to organize in-Family (semi)permanent parties DPS based, this means that you can stay with players in {HW-INITIALS} if your DPS is similar to their DPS.

Being a {NMTC-INITIALS} member you are automatically part of our waiting list for {HW-INITIALS}. The advantage is that you already know how we work together, this is the reason because we prefer to promote from there instead of getting foreign players, we can also help you to grow faster and complete every challenge you need.
Another reason to join us is that if you already have the requirements to join {HW-INITIALS} but there are no spots, you are making a reservation, in fact we promote everytime the member in {NMTC-INITIALS} who best fits the requirements for {HW-INITIALS}.

{NMTC-MEMBERSCOUNTSTRING}
{NMTC-LEVELFF}
{NMTC-WISHINGWELLFF}
{NMTC-IOUSCORE}
{NMTC-AVERAGELEVEL}
{NMTC-BUILDINGSLIST-TEXT}

[hr]


[align=center][img]https://s26.postimg.cc/41t35xoyh/The_Necromancy_Background-forum.png[/img][/align]
[size=large][b]{TN-GUILD}[/b] - {TN-DYNAMICSLOTS} - Entry level {TN-ENTRYLEVEL}[/size]
We are ranked {TN-POSITION}, with your support we can easily break on top {TN-RANKTARGET}!
We are looking for active players who will use our forum and update their progress on our spreadsheet at least once a week(it requires ~5 minutes), these things allow us to organize in-Family (semi)permanent parties DPS based, this means that you can stay with players in {NMTC-INITIALS} if your DPS is similar to their DPS.

Being a {TN-INITIALS} member you are automatically part of our waiting list for {NMTC-INITIALS}. The advantage is that you already know how we work together, this is the reason because we prefer to promote from there instead of getting foreign players, we can also help you to grow faster and complete every challenge you need.
Another reason to join us is that if you already have the requirements to join {NMTC-INITIALS} but there are no spots, you are making a reservation, in fact we promote everytime the member in {TN-INITIALS} who best fits the requirements for {NMTC-INITIALS}.
[b]There is a carry offer for who join {TN-INITIALS}, the carry is level 1242 with 4T dps.[/b]

{TN-MEMBERSCOUNTSTRING}
{TN-LEVELFF}
{TN-WISHINGWELLFF}
{TN-IOUSCORE}
{TN-AVERAGELEVEL}
{TN-BUILDINGSLIST-TEXT}

[hr]
[align=center][b]{HWF-NAME}[/b] - It's Bunga Bunga time![/align]

[size=xx-small]
Notes:[/size]
[list]
[*][size=xx-small]This post is autogenerated and updated frequently, check the warriors counter above the warriors list to know if there are free spots, or the Guild Hall upgrade bar to guess when next spot will be available, some addidional spots for replacements can be found in the last post.[/size]
[*][size=xx-small]We prefer to grow players in {TN-INITIALS} and then promote them to {NMTC-INITIALS} and {HW-INITIALS} instead of getting external players, if you are planning to stay in a top guild, you should [url=https://iourpg.com/forum/private.php?action=send&uid=829]PM me[/url] for more info right now.[/size]
[/list]
```

```markdown
![](http://s22.postimg.cc/i2cff57fl/hwfamily_logo_black.png)

# **{HWF-NAME}**
  
  
 We offer to any member of our Family a complete and working platform that help you in various aspects of the game.  
 The platform is the Family’s organization, a complex system which includes roles, rules, parties, communication, coordination, democracy, collaboration and so on.  
 If you play 24/7 we can grant you a permanent party FOREVER with similar DPS, we take care of you and your party mates making your party always working.  
  
 A little introduction to Holy Warriors Family can be found [here](http://pastebin.com/raw.php?i=7XF82u4g).  
  
 If you want join us, [send me a PM](http://www.kongregate.com/accounts/turidrum/private_messages?focus=true).
 We use Family Points(FP) to allocate players in our guilds, you can't know how they work until you join us.
  
{HWF-RANKINGSCHART}

* * *

![](http://s24.postimg.cc/rfftet4ed/HW_logo_forum.png)

# **{HW-GUILD}** – {HW-DYNAMICSLOTS} – Entry level {HW-ENTRYLEVEL}
  

We are ranked {HW-POSITION}, with your support we can easily break on top {HW-RANKTARGET}!  
 We are looking for active players who will use our forum and update their progress on our spreadsheet at least once a week(it requires ~5 minutes), these things allow us to organize in-Family (semi)permanent parties DPS based.

{HW-MEMBERSCOUNTSTRING}
{HW-LEVELFF}
{HW-WISHINGWELLFF}
{HW-IOUSCORE}
{HW-AVERAGELEVEL}
{HW-BUILDINGSLIST-TEXT}

* * *

![](http://s4.postimg.cc/6gp80wmct/NMTC_logo_forum.png)

# **{NMTC-GUILD}** – {NMTC-DYNAMICSLOTS} – Entry level {NMTC-ENTRYLEVEL}
  

We are ranked {NMTC-POSITION}, with your support we can easily break on top {NMTC-RANKTARGET}!  
 We are looking for active players who will use our forum and update their progress on our spreadsheet at least once a week(it requires ~5 minutes), these things allow us to organize in-Family (semi)permanent parties DPS based, this means that you can stay with players in {HW-INITIALS} if your DPS is similar to their DPS.  
  
 Being a {NMTC-INITIALS} member you are automatically part of our waiting list for {HW-INITIALS}. The advantage is that you already know how we work together, this is the reason because we prefer to promote from there instead of getting foreign players, we can also help you to grow faster and complete every challenge you need.  
 Another reason to join us is that if you already have the requirements to join {HW-INITIALS} but there are no spots, you are making a reservation, in fact we promote everytime the member in {NMTC-INITIALS} who best fits the requirements for {HW-INITIALS}.

{NMTC-MEMBERSCOUNTSTRING}
{NMTC-LEVELFF}
{NMTC-WISHINGWELLFF}
{NMTC-IOUSCORE}
{NMTC-AVERAGELEVEL}
{NMTC-BUILDINGSLIST-TEXT}


* * *

![](https://s26.postimg.cc/41t35xoyh/The_Necromancy_Background-forum.png)

# **{TN-GUILD}** – {TN-DYNAMICSLOTS} – Entry level {TN-ENTRYLEVEL}
  

We are ranked {TN-POSITION}, with your support we can easily break on top {TN-RANKTARGET}!  
 We are looking for active players who will use our forum and update their progress on our spreadsheet at least once a week(it requires ~5 minutes), these things allow us to organize in-Family (semi)permanent parties DPS based, this means that you can stay with players in HW if your DPS is similar to their DPS.  
  
 Being a {TN-INITIALS} member you are automatically part of our waiting list for {NMTC-INITIALS}. The advantage is that you already know how we work together, this is the reason because we prefer to promote from there instead of getting foreign players, we can also help you to grow faster and complete every challenge you need.  
 Another reason to join us is that if you already have the requirements to join {NMTC-INITIALS} but there are no spots, you are making a reservation, in fact we promote everytime the member in {TN-INITIALS} who best fits the requirements for {NMTC-INITIALS}.
**There is a carry offer for who join {TN-INITIALS}, the carry is level 1242 with 4T dps.**

{TN-MEMBERSCOUNTSTRING}
{TN-LEVELFF}
{TN-WISHINGWELLFF}
{TN-IOUSCORE}
{TN-AVERAGELEVEL}
{TN-BUILDINGSLIST-TEXT}

* * *

**{HWF-NAME}** – It’s Bunga Bunga time!

###### Notes:

  
- This post is autogenerated and updated frequently, check the warriors counter above the warriors list to know if there are free spots, or the Guild Hall upgrade bar to guess when next spot will be available, some addidional spots for replacements can be found in the last post.
  
- We prefer to grow players in {TN-INITIALS} and then promote them to {NMTC-INITIALS} and {HW-INITIALS} instead of getting external players, if you are planning to stay in a top guild, you should [PM me](http://www.kongregate.com/accounts/turidrum/private_messages?focus=true) for more info right now.
```

The `title.template` file is for the generation of the title of the thread, is used as template with some special words that will be replaced by guild's data, there is an example file with almost all the available special words:

```
-=[{HWF-NAME} recruiting]=- -=[{HW-INITIALS} #{HW-RANK} L{HW-LEVEL} + {NMTC-INITIALS} #{NMTC-RANK} L{NMTC-LEVEL} + {TN-INITIALS} #{TN-RANK} L{TN-LEVEL}]=-
```

The above title example will output something like this:

> -=[Holy Warriors Family recruiting]=- -=[HW #25 L201 + NMTC #34 L118 + TN #54 L98]=-


---

## Template keywords

Supposing you set "HW" as value for `guild_initials` and "HWF" as value for `family_initials` in your file `config.json`, then you can use the following keywords in your templates

- `{HWF-INITIALS}` coalition initials.
- `{HWF-NAME}` coalition full name.
- `{HWF-RANKINGSCHART}` coalition ranking chart.
- `{HW-INITIALS}` guild initials.
- `{HW-GUILD}` guild full name.
- `{HW-RANK}` guild rank.
- `{HW-POSITION}` guild rank as 1st, 2nd, 3rd etc.
- `{HW-RANKTARGET}` guild rank goal.
- `{HW-ENTRYLEVEL}` guild minimum requirements.
- `{HW-BUILDINGS}` guild building levels separated by slashes(/).
- `{HW-MEMBERS}` guild number of members.
- `{HW-MAXMEMBERS}` guild max number of members.
- `{HW-EMPTYSLOTS}` guild free spots.
- `{HW-DYNAMICSLOTS}` guild free spots, a string like this "3 players" or if there are no slots "Full".
- `{HW-IOUSCORE}` guild iou score.
- `{HW-AVERAGELEVEL}` guild average level.
- `{HW-AVERAGEIOUSCORE}` guild average iou score.
- `{HW-MEMBERSLIST}` guild list of members.
- `{HW-BUILDINGSLIST}` guild pretty printed buildings list, images as upgrade status bars.[^1]
- `{HW-BUILDINGSLIST-TEXT}` guild pretty printed buildings list, upgrade status as text percentage.
- `{HW-MEMBERSCOUNTSTRING}` guild number of members related to spots available, for example 35/40.
- `{HW-LEVEL}` guild level.
- `{HW-WISHINGWELL}` guild wishing well.
- `{HW-LEVELFF}` guild level in a different format.
- `{HW-WISHINGWELLFF}` wishing well in a different format.


[^1] Due to forum restriction on iou forum, a max of 10 images can stay in a single post, eventually fallback on `{HW-BUILDINGSLIST-TEXT}`.

---

## Usage

To generate the post, on *nix system you have to type this:

```console
$ ./IOURecruitsManager.py
```
If you want the kong result:

```console
$ ./IOURecruitsManager.py --kong
```

Just copy the output and paste in your guild's thread, but don't forget to paste after clicked the sourcecode icon(or pressed ctrl+shift+s), infact the above script will give you sourcecode output for forum.
If you want generate the title, on *nix system you have to type this:

```console
$ ./IOURecruitsManager.py --title
```

To show a recruiting string, type:

```console
$ ./IOURecruitsManager.py --recruiting_string
```

This should output an useful string to recruit new players in chat, like this:

> -=[Holy Warriors Family]=-=[TOP 14]=-=[5 free spots 250+]=- IOU: https://iourpg.com/forum/showthread.php?tid=1206 KONG: http://www.kongregate.com/forums/12180-guild-recruitment/topics/543389

There are even shorthands for each argument you can use in the terminal, to show the complete arguments list type:

```console
$ ./IOURecruitsManager.py --help
```

This is a terminal output given with `--help`:

```console
--------------------------------------------------------------------------------
IOU Recruits Manager 1.3.1
--------------------------------------------------------------------------------

Description: Helper tool for managing Idle Online Universe's recruiting threads

Usage: ./IOURecruitsManager.py [ -i | -k | -t | -r | -v | -h ]
-i --iou                       Output format for IOU forum, default.                       
-k --kong                      Output format for Kongragate forum(html)                    
-t --title                     Prints the thread's title.                                  
-r --recruiting_string         Prints an oneliner, plain text, recruiting string.          
-u --update                    Updates guild's data with data from the game command /export.
-v --version                   Prints program version.                                     
-h --help                      Show this help.                                             

Without arguments generates the full post.
More info: https://iourpg.com/forum/showthread.php?tid=1350
```

Manually copying the results can be annoying... better to use xclip to store the output in the clipboard, as in this example for kongregate:

```console
$ ./IOURecruitsManager.py -k | xclip -selection clipboard
```

Have fun.

---

###### Note:

This documentation may be incomplete or outdated, use `./IOURecruitsManager.py --help` for more info.
