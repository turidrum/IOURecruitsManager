#!/usr/bin/python3
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
#  
#  IOU Recruits Manager (C) 2015-2018 turidrum
#  
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------------------

import atexit
import os
import readline
import math
import json



class Importer:

    def __init__(self, configFile):
        self.configFile = configFile
    

    def getBuildLevelCost(self, lvl):
        """
        Return the stones needed for upgrading the building to next level
        
        @param lvl                      integer representing the current level of the building
        """
        nextLevel = lvl + 1
        Q = 5 * (1 + int(lvl/50))
        return int(1000 * (math.pow(nextLevel,2)) * math.ceil(nextLevel/10) * Q)
    
    
    def getBuildingStatus(self, totalStones):
        """
        Calculates the level of a building and the stones required for the next upgrade
        
        @param totalStones              the amount of stones invested in this building
        @return list                    a list containing the level of the building and the stones required for the next upgrade
        """
        lvl = 0
        buildLevelCost = 0
        doUpgrades = 1
        while doUpgrades:
            buildLevelCost = self.getBuildLevelCost(lvl)
            if (totalStones >= buildLevelCost):
                totalStones = totalStones - buildLevelCost
                lvl += 1
            else:
                doUpgrades = 0
                
        stonesToNextLevel = buildLevelCost - totalStones
        return [lvl, stonesToNextLevel]
    
    
    def composeBuilding(self, n, s):
        status = self.getBuildingStatus(s)
        building = [n, status[0], status[1], s] # name, level, stones to next level, total stones
        return building;
    
    
    def parse(self):
        data = json.loads(input("Paste here the data collected in game using the console command /export: "))
        #print(json.dumps(data, sort_keys=True, indent=4))
        #exit(0)
        import_data = {
	    "members": [],
	    "buildings": [],
	    "guildLevel": 0,
	    "goldLevel": 0
        }
        
        for m in data["guildData"]["members"]:
            name = m["name"]
            role = m["rank"]
            if role == 3:
                role = "Creator"
            elif role == 2:
                role = "Leader"
            elif role == 1:
                role = "Captain"
            else:
                role = "Member"
            
            level = m["level"]
            iouscore = m["iou"]
            networth = m["networth"]
            import_data["members"].append([name, role, level, iouscore, networth])
            
        import_data["buildings"].append(self.composeBuilding("Altar", data["guildData"]["Investments"]["Altar"]))
        import_data["buildings"].append(self.composeBuilding("Fortress", data["guildData"]["Investments"]["Fortress"]))
        import_data["buildings"].append(self.composeBuilding("Warehouse", data["guildData"]["Investments"]["Warehouse"]))
        import_data["buildings"].append(self.composeBuilding("Sacrificial Tower", data["guildData"]["Investments"]["SacrificialTower"]))
        import_data["buildings"].append(self.composeBuilding("Bank", data["guildData"]["Investments"]["Bank"]))
        import_data["buildings"].append(self.composeBuilding("Sawmill", data["guildData"]["Investments"]["Sawmill"]))
        import_data["buildings"].append(self.composeBuilding("Library", data["guildData"]["Investments"]["Library"]))
        import_data["buildings"].append(self.composeBuilding("Aquatic Research", data["guildData"]["Investments"]["Aqua"]))
        import_data["buildings"].append(self.composeBuilding("Stables", data["guildData"]["Investments"]["Stable"]))
        import_data["buildings"].append(self.composeBuilding("Space Academy", data["guildData"]["Investments"]["Spatial"]))
        
        import_data["guildLevel"] = data["guildData"]["level"]
        import_data["goldLevel"] = data["guildData"]["Well"]["goldlv"] / 10
        
        guild = data["guildData"]["name"]
        return [guild, import_data];

    def update(self, guild, data):
        with open(self.configFile, 'r+') as json_data:
            config = json.load(json_data)
            for i, g in enumerate(config["guilds"]):
                if g[0]["guild_name"] == guild:
                    config["guilds"][i][0]["import"] = data
                    
            json_data.seek(0)
            json_data.write(json.dumps(config, sort_keys=False, indent=4))
            json_data.truncate()
            print("The data for {} has been imported.".format(guild))

    
if __name__ == "__main__":
    i = Importer("config.json")
    #print(i.parse()[0])
    guild, data = i.parse()
    i.update(guild, data)

