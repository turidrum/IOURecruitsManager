#!/usr/bin/python3
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
#  
#  IOU Recruits Manager (C) 2015-2018 turidrum
#  
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------------------


# useful resources:
# https://developers.google.com/chart/image/docs/chart_playground
# https://developers.google.com/chart/image/docs/chart_params
# an example:
#  # http://chart.googleapis.com/chart?cht=lc&chs=450x350&chtt=Holy+Warriors+Family+Rankings&chdl=HW|NMTC&chdlp=b&chd=t:9,12,11,10,7,10,9|61,60,59,62,59,56,56&chls=2|2&chma=25,25,25,25&chxt=x,y&chxl=0:|-7+days|-6+days|-5+days|-4+days|-3+days|-2+days|-1+days|NOW&chxs=0,ff0000,12,0,lt|1,0000cc,12,1,lt&chxr=1,200,1,10|0,-7,0,1&chds=200,1&chco=3399ff,ff9933&chg=14.28,5.03,1,5,0,0&chof=png

import json
from random import shuffle, randint
import time
import datetime

class Chart:
    def __init__(self, forum):
        """
        prepare the chart with some dummy data
        """
        self.forum = forum
        self.dataFile = "chart_data.json"
        self.baseURL = "http://chart.googleapis.com/chart?"
        self.parameters = {
            "cht": "lc",                   # chart type
            "chf": "bg,s,333333",          # background
            "chs": "450x350",              # chart size
            "chtt": "Chart Title",         # chart title
            "chts": "cccccc,12,c",         # chart title color, size, alignment
            "chdl": "Guild1|Guild2|GuldN", # data labels
            "chdls": "cccccc,12",          # labels color and size
            "chd": "t:1,2,3,4,5|6,7,8,9,10|11,12,13,14,15", # chart data
            "chls": "2|2|2",       # labels style
            "chma": "25,25,25,25", # chart margins
            "chxt": "x,y",         # axis type
            "chxl": "0:|-7+days|-6+days|-5+days|-4+days|-3+days|-2+days|-1+days|NOW", # axis labels
            "chxs": "0,ff9933,10,0,lt|1,99ff33,10,1,lt", # axis styles
            "chxr": "1,201,0,8|0,-7,0,1",               # axis ranges
            "chds": "200,1",                             # data ranges
            "chco": "3399ff,ff9933,99ff33",              # data colors
            "chg": "14.28,8.03,3,3,0,0",                 # grid
            "chof": "png"       # image output format
        }
        

    def getChartUrl(self):
        """
        return a formatted url
        """
        url = self.baseURL + "&".join(["%s=%s" % (k,self.parameters[k]) for k in self.parameters.keys()])
        if self.forum == "iou":
            return "[align=center][img]%s[/img][/align]" % url
        elif self.forum == "kong":
            return "![](%s)" % url
        else:
            return "[align=center][img]%s[/img][/align]" % url
            #return self.baseURL + "&".join(["%s=%s" % (k,self.parameters[k]) for k in self.parameters.keys()])


    def setTitle(self, title):
        self.parameters["chtt"] = "+".join(title.split(" "))

    def setGuildName(self, guild):
        self.parameters["chdl"] = "|".join(guild)
        self.parameters["chls"] = "|".join(["2" for x in guild])

    def setGuildData(self, data):
        self.parameters["chd"] = "t:" + "|".join(data)
        #self.parameters["chd"] = "t:" + str(data)
        #self.parameters["chco"] = ",".join(self.getColors(len(data)))
        
    def setGuildsColors(self, guilds_colors):
        i = 0
        colors = []
        for g in self.parameters["chdl"].split("|"):
            if g in guilds_colors[0]:
                i = guilds_colors[0].index(g)
                colors.append(guilds_colors[1][i])
            else:
                colors.append(self.getColors(2))
        self.parameters["chco"] = ",".join(colors)

        
        
    def getColors(self, n):
        steps = ["0","3","6","9","c","f"]
        colors = []
        for i in range(0, n):
            color = "%s%s%s%s%s%s" % (steps[randint(0,5)],steps[randint(0,5)],steps[randint(0,5)],steps[randint(0,5)],steps[randint(0,5)],steps[randint(0,5)])
            colors.append(color)
        return colors
        
        
    def setTimeAxis(self, data):
        "sets the time axis trying to summarize the dates in eight columns"
        columns = 8
        udata = list(set(data))
        udata.sort(reverse=True)
        relevant = len(udata)/columns
        if relevant > 1.0:
            count = 0
            summarized = []
            summarized.append(udata.pop(0))
            summarized.append(udata.pop())
            skip = -2
            while len(summarized) != columns:
                skip += int(relevant)
                summarized.append(udata.pop(skip))
            udata = summarized
            udata.sort(reverse=True)

        axis = [datetime.datetime.fromtimestamp(ts).strftime("%m-%d-%y") for ts in udata[::-1]] # gets the last value by inverting
        if len(axis) < columns:
            axis = [""]*(columns-len(axis))+axis # fills with empty values
        self.parameters["chxl"] ="0:|%s" % "|".join(axis)
        

        
        
    def loadData(self):
        json_file = open(self.dataFile)
        data = json.load(json_file)
        json_file.close()
        guilds = [x for x in data["guilds"].keys()]
        self.setGuildName(guilds)
        rankings = []
        tsdata = []
        for g in data["guilds"]:
            #granks = ""
            #[str(data["guilds"][x][1]) for x in data["guilds"]]
            #granks = [x[1] for x in data["guilds"][g]]
            granks = ",".join(["%d" % x[1] for x in data["guilds"][g]][-60:])
            tsdata.extend([x[0] for x in data["guilds"][g]][-60:])
            rankings.append(granks)
        
        self.setTimeAxis(tsdata)
        self.setGuildData(rankings)
            
    def saveData(self, data):
        ts = time.time()
        with open(self.dataFile, "r") as json_file:
            json_file.seek(0)
            if json_file.read(1):
                json_file.seek(0)
                storedData = json.load(json_file)
            else:
                storedData = {"guilds":{}}
                storedData["guilds"] = dict([(x,[[ts,y]]) for x,y in data])
                #print(json.dumps(storedData))
            # checks that only one entry at day will be done, overriding previous entry at the same day
            for g in data:
                if g[0] in storedData["guilds"]: # if there is a new guild, it has no previous stored data
                    lastEntryDay = datetime.datetime.fromtimestamp(storedData["guilds"][g[0]][-1][0]).strftime("%m-%d-%y")
                else:
                    lastEntryDay = False
                    storedData["guilds"].update({g[0]: [[ts,g[1]]]})
                currentEntryDay = datetime.datetime.fromtimestamp(ts).strftime("%m-%d-%y")
                if lastEntryDay and lastEntryDay == currentEntryDay:
                    # remove last entry, it will be updated by the current entry
                    del storedData["guilds"][g[0]][-1]
                
                
        with open(self.dataFile, "w") as json_file:
            for g in data:
                storedData["guilds"][g[0]].append([ts, g[1]])
            #json.dump(storedData, json_file, indent=4, separators=(',', ':'))
            json.dump(storedData, json_file)
            
            

if __name__ == "__main__":
    c = Chart()
    d1 = [7,7,7,8,8,8,8,8,9,9,9,9,9,9,9]
    d2 = [52,53,54,55,55,56,56,56,57,57,57,58,58,59,60]
    d3 = [149,149,149,151,151,155,157,158,159,159,160,161,161,162,163]
    #shuffle(d1)
    #shuffle(d2)
    #shuffle(d3)
    #data1 = ",".join(["%d" % x for x in d1])
    #data2 = ",".join(["%d" % x for x in d2])
    #data3 = ",".join(["%d" % x for x in d3])
    c.setTitle("Holy Warriors Family Rankings")
    c.setGuildName(["HW", "NMTC", "SD"])
    #c.setGuildData([data1,data2,data3])
    shuffle(d1)
    shuffle(d2)
    shuffle(d3)
    #c.dataFile = "chart_data_test.json"
    c.saveData([["HW",d1[0]],["NMTC",d2[0]],["SD",d3[0]]])
    c.loadData()
    print(c.getChartUrl())
