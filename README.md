# IOU Recruits Manager



## About IOU Recruits Manager

Manager of recruiting threads for idle online universe, works for iou and kongregate forums.

Features:

- Works for IOU and Kongregate forums.
- Easy to edit guild data and import from game.
- Multiguild support.
- Separated templates for IOU and Kongregate.
- Recruiting string generation, useful for chat recruiting and discord.

---

## Documentation

The documentation is [here](https://gitlab.com/turidrum/IOURecruitsManager/blob/master/docs/documentation.md).

---

## License

This program is free software and released under [GPLv2](https://gitlab.com/turidrum/IOURecruitsManager/blob/master/LICENSE.md).
