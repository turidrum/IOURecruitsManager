#!/usr/bin/python3
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
#  
#  IOU Recruits Manager (C) 2015-2018 turidrum
#  
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------------------

import math


class Building:

        # ##########################################################################
        # NOTE:
        # update this from http://iourpg.wikia.com/wiki/Guild_Buildings
        # ##########################################################################
        def getBuildLevelCost(self, lvl):
                """
                Return the stones needed for upgrading the building to next level
        
                @param lvl                        integer representing the current level of the building
                """
                nextLevel = lvl + 1
                Q = 5 * (1 + int(lvl/50))
                return int(1000 * (math.pow(nextLevel,2)) * math.ceil(nextLevel/10) * Q)
        


        bars = { # links to images corresponding to percentage
                0: "https://upload.wikimedia.org/wikipedia/commons/c/cf/Gasr0percent.png",
                5: "https://upload.wikimedia.org/wikipedia/commons/9/90/Gasr5percent.png",
                10: "https://upload.wikimedia.org/wikipedia/commons/5/5b/Gasr10percent.png",
                15: "https://upload.wikimedia.org/wikipedia/commons/c/c8/Gasr15percent.png",
                20: "https://upload.wikimedia.org/wikipedia/commons/6/62/Gasr20percent.png",
                25: "https://upload.wikimedia.org/wikipedia/commons/0/08/Gasr25percent.png",
                30: "https://upload.wikimedia.org/wikipedia/commons/6/61/Gasr30percent.png",
                35: "https://upload.wikimedia.org/wikipedia/commons/4/47/Gasr35percent.png",
                40: "https://upload.wikimedia.org/wikipedia/commons/3/3e/Gasr40percent.png",
                45: "https://upload.wikimedia.org/wikipedia/commons/0/0e/Gasr45percent.png",
                50: "https://upload.wikimedia.org/wikipedia/commons/a/a8/Gasr50percent.png",
                55: "https://upload.wikimedia.org/wikipedia/commons/2/27/Gasr55percent.png",
                60: "https://upload.wikimedia.org/wikipedia/commons/e/e7/Gasr60percent.png",
                65: "https://upload.wikimedia.org/wikipedia/commons/9/96/Gasr65percent.png",
                70: "https://upload.wikimedia.org/wikipedia/commons/7/70/Gasr70percent.png",
                75: "https://upload.wikimedia.org/wikipedia/commons/7/77/Gasr75percent.png",
                80: "https://upload.wikimedia.org/wikipedia/commons/4/4d/Gasr80percent.png",
                85: "https://upload.wikimedia.org/wikipedia/commons/9/9a/Gasr85percent.png",
                90: "https://upload.wikimedia.org/wikipedia/commons/b/bd/Gasr90percent.png",
                95: "https://upload.wikimedia.org/wikipedia/commons/7/78/Gasr95percent.png",
                100: "https://upload.wikimedia.org/wikipedia/commons/f/f3/Gasr100percent.png"
        }

        @staticmethod
        def getInstance(buildName, buildLevel, stonesNeeded, forum):
                """
                factory
                """
                if forum == "kong":
                        # get kong instance
                        b = BuildingKong(buildName, buildLevel, stonesNeeded)
                elif forum == "iou":
                        # get iou instance
                        b = BuildingIOU(buildName, buildLevel, stonesNeeded)
                else:
                        # get default instance
                        b = BuildingIOU(buildName, buildLevel, stonesNeeded)
                        
                b.setUpgradePercent(stonesNeeded)
                b.setPercentURL()
                return b
                


        def round_to_5(self, n):
                """
                Rounds a number to nearest 5

                @param integer				the number to round
                @return integer				the rounded number
                """

                return int(round(n*2,-1)/2)


        def setUpgradePercent(self, stonesNeeded):
                """
                Sets the percentage upgraded status of a building

                @param integer				the building's current level
                @param integer				the stones needed to the next building level
                """

                nextBuildLevel = self.getBuildLevelCost(self.buildLevel)
                
                if nextBuildLevel-stonesNeeded < 0:
                        percent = 0
                else:
                        percent = int(((nextBuildLevel-stonesNeeded)*100)/nextBuildLevel)
                
                self.upgradeStatusPercent = percent


        def setPercentURL(self):
                """
                Sets the image URL of building's percentage upgraded status
                """
                url = self.bars[self.round_to_5(self.upgradeStatusPercent)]
                self.percBarURL = url


        def getBuildingProperties(self):
                if self.buildName == "Guild Hall":
                        return "Unknown Bonus"
                if self.buildName == "Bank":
                        return "Gold rate: {}%".format(self.buildLevel)
                if self.buildName == "Fortress":
                        return "Ascension Points: %d" % (self.buildLevel)
                if self.buildName == "Altar":
                        return "XP Rate: %1.1f%%" % (self.buildLevel * 0.5)
                if self.buildName == "Stables" or self.buildName == "Stable":
                        return "Pet Damage: %1.1f%%" % (self.buildLevel * 0.3)
                if self.buildName == "Warehouse":
                        return "Stones from Mining: %1.1f%%" % (self.buildLevel * 1.50)
                if self.buildName == "Sacrificial Tower":
                        return "Orb XP: %1.1f%%" % (self.buildLevel * 0.4)
                if self.buildName == "Sawmill":
                        return "Woodcutting Yield: %d%%" % (self.buildLevel * 3)
                if self.buildName == "Library":
                        return "Card Yield: %1.1f%%" % (self.buildLevel * 1.5)
                if self.buildName == "Aquatic Research":
                        return "Fish Value: %d%%" % (self.buildLevel * 2)
                if self.buildName == "Space Academy":
                        return "Ship AI Damage/HP: %1.1f%%" % (self.buildLevel * 0.5)







class BuildingIOU(Building):
        
        def __init__(self, buildName, buildLevel, stonesNeeded):
                self.upgradeStatusPercent = 0 # current percentage of building update
                self.percBarURL = "" # URL of the image that match the building upgrade status
                self.buildName = buildName.title() # name of the building
                self.buildLevel = buildLevel # current build level


        def buildingFF(self, method="image"):
                """
                Prints a row for the guild's thread that contains building info
                """
                if self.buildName == "Bank":
                        method = "text"
                if method == "text":
                        return "{:<40s} {:<40s} {:<110s}".format("[*][font=Courier New]%s" % self.buildName, ("Lv: %d (%s)") % (self.buildLevel, self.getBuildingProperties()),"Upgrade status: %1.0f%%[/font]" % (self.upgradeStatusPercent))
                elif method == "image":
                        return "{:<40s} {:<40s} {:<110s}".format("[*][font=Courier New]%s" % self.buildName, ("Lv: %d (%s)") % (self.buildLevel, self.getBuildingProperties()),"Upgrade status: [[img=119x8]%s[/img]][/font]" % (self.percBarURL))
                        





class BuildingKong(Building):
        
        def __init__(self, buildName, buildLevel, stonesNeeded):
                self.upgradeStatusPercent = 0 # current percentage of building update
                self.percBarURL = "" # URL of the image that match the building upgrade status
                self.buildName = buildName.title() # name of the building
                self.buildLevel = buildLevel # current build level
                
                
        def buildingFF(self, method="image"):
                """
                Prints a row for the guild's thread that contains building info
                """
                if self.buildName == "Bank":
                        method = "text"
                if method == "text":
                        return "{:<22s} {:<38s} {:<5s}".format("```\n%s" % self.buildName, ("Lv: %d (%s)") % (self.buildLevel, self.getBuildingProperties()),"Upgrade status: %1.0f%%\n```\n" % (self.upgradeStatusPercent,))
                elif method == "image":
                        return "{:<22s} {:<38s} {:<100s}".format("```\n%s" % self.buildName, ("Lv: %d (%s)") % (self.buildLevel, self.getBuildingProperties()),"Upgrade status: %1.0f%%\n```\n![](%s)\n" % (self.upgradeStatusPercent, self.percBarURL))
                                




if __name__ == "__main__":
        print("-"*80)
        print("Test: iou's forum output:")
        buildIOU = Building.getInstance("Guild Hall", 25, 15500000, "iou")
        print(buildIOU.buildingFF())
        print("-"*80)
        print("Test: kongregate's forum output:")
        buildKong = Building.getInstance("Guild Hall", 25, 15500000, "kong")
        print(buildKong.buildingFF())
        print("-"*80)
        
        
