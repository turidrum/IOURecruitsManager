#!/usr/bin/python3
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
#  
#  IOU Recruits Manager (C) 2015-2018 turidrum
#  
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------------------



class Guild:
    
    def __init__(self, guildName, guildInitials, guildRank, rankTarget, entryLevel, buildings, color):
        """
        constructor
        """
        self.members = []
        self.guildName = guildName
        self.guildInitials = guildInitials
        self.guildRank = guildRank
        self.rankTarget = rankTarget
        self.entryLevel = entryLevel
        self.buildings = buildings
        self.guildColor = color
        self.setMaxMembers(40)
        self.level = 0
        self.wishingwell = 0
        self.forum = "iou"
        
    def setMaxMembers(self, maxMembers):
        """
        Sets the max members number for the guild
        """
        
        self.maxMembers = maxMembers
    
    def addMember(self, name, status, level, IOUScore):
        """
        Adds a member to the guild
        """
        
        self.members.append([name, status, level, IOUScore])

    def getAverageLevel(self):
        """
        Calc the average level of guild's members
        
        @return float				the average level of guild's members
        """
        
        sumlevels = 0.0
        for m in self.members:
            sumlevels += m[2]
        return float(sumlevels/len(self.members))
    
    def getGuildIOUScore(self):
        """
        Calc the guild's IOU score
        
        @return integer				the guild's IOU score
        """
        
        sumIOU = 0
        for m in self.members:
            sumIOU += m[3]
        return sumIOU
    
    def guildLevelFF(self):
        """
        @return string				the guild's level
        """
        
        if self.forum == "kong":
            return "**%s** Guild level: **%d**\n" % (self.guildName, self.level)
        return "[b]%s[/b] Guild level: [b]%d[/b]\n" % (self.guildName, self.level)
    
    def wishingWellFF(self):
        """
        @return string				the wishing well level
        """
        
        if self.forum == "kong":
            return "**%s** Wishing well: **%.1f**\n" % (self.guildName, self.wishingwell)
        return "[b]%s[/b] Wishing well: [b]%.1f[/b]\n" % (self.guildName, self.wishingwell)

    def getAverageIOUScore(self):
        """
        Calc the average IOU score of guild's members
        
        @return float				the average IOU score of guild's members
        """
        
        return float((0.0+self.getGuildIOUScore())/len(self.members))
    
    def guildIOUScoreFF(self):
        """
        @return string				the guild's IOU Score
        """
        
        if self.forum == "kong":
            return "**%s** IOU Score: **%d**\n" % (self.guildName, self.getGuildIOUScore())
        return "[b]%s[/b] IOU Score: [b]%d[/b]\n" % (self.guildName, self.getGuildIOUScore())
    
    def averageLevelFF(self):
        """
        @return string				the average level of guild's members
        """
        
        if self.forum == "kong":
            return "**%s** average level: **%.2f**\n" % (self.guildName, float(self.getAverageLevel()))
        return "[b]%s[/b] average level: [b]%.2f[/b]\n" % (self.guildName, float(self.getAverageLevel()))
    
    def averageIOUScoreFF(self):
        """
        @return string				the average IOU Score of guild's members
        """
        
        if self.forum == "kong":
            return "<strong>%s</strong> average IOU: <strong>%.2f</strong>\n" % (self.guildName, float(self.getAverageIOUScore()))
        return "[b]%s[/b] average IOU: [b]%.2f[/b]\n" % (self.guildName, float(self.getAverageIOUScore()))


    def membersCountString(self):
        """
        @return string          # a counter for the amount of mambers in the guild
        """
        if self.forum == "kong":
            rvalue = "%d/%d" % (len(self.members), self.maxMembers) if len(self.members) < self.maxMembers else "%d/%d" % (len(self.members), self.maxMembers)
            rvalue = "**%s (%s)**\n" % (self.guildName, rvalue)
        else:
            rvalue = "[color=#33ff33]%d[/color]/%d" % (len(self.members), self.maxMembers) if len(self.members) < self.maxMembers else "[color=#ff3333]%d[/color]/%d" % (len(self.members), self.maxMembers)
            rvalue = "[b]%s (%s)[/b]\n" % (self.guildName, rvalue)
        return rvalue
    
    def membersListFF(self):
        """
        @return string				the members list
        """
        
        if self.forum == "kong":
            membersCountString = "%d/%d" % (len(self.members), self.maxMembers) if len(self.members) < self.maxMembers else "%d/%d" % (len(self.members), self.maxMembers)
            
            result = "**%s (%s)**:\n" % (self.guildName, membersCountString)
            result += "```\n"
            for m in self.members:
                result += u'{:<40s} {:<8s} {:<9s} {:<8s}'.format("%s" % m[0], "[%s" % m[1], "| Lv %d" % m[2], "| IOU %d]\n" % m[3])
            result += "```\n\n"
            
            return result
            
        membersCountString = "[color=#33ff33]%d[/color]/%d" % (len(self.members), self.maxMembers) if len(self.members) < self.maxMembers else "[color=#ff3333]%d[/color]/%d" % (len(self.members), self.maxMembers)
        
        result = "[b]%s (%s)[/b]:\n" % (self.guildName, membersCountString)
        result += "[list]\n"
        for m in self.members:
            result += u'{:<40s} {:<8s} {:<9s} {:<8s}'.format("[*][font=Courier New]%s" % m[0], "[%s" % m[1], "| Lv %d" % m[2], "| IOU %d][/font]\n" % m[3])
        result += "[/list]\n\n"
        
        return result
    
    def buildingsListFF(self, method):
        """
        @return string				the buildigs list
        """
        
        # buildigs data forum formatted
        if self.forum == "kong":
            result = "**%s** Buildings:\n" % self.guildName
            #result += "<pre>\n"
            for b in self.buildings:
                result += b.buildingFF(method)
            #result += "</pre>\n\n"
            
            return result
        
        result = "[b]%s[/b] Buildings:\n" % self.guildName
        result += "[list]\n"
        for b in self.buildings:
            result += b.buildingFF(method)
        result += "[/list]\n\n"
    
        return result
