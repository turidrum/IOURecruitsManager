#!/usr/bin/python3
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
#  
#  IOU Recruits Manager (C) 2015-2018 turidrum
#  
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# ------------------------------------------------------------------------------




class TemplateParser:

    def __init__(self, family, forum):
        """
        Constructor
        
        @param list					a list that contains instances of Guild class
        @param string				the desired forum output (iou|kong)
        """
        
        self.words = {}
        totalEmptySpots = 0
        
        for guild in family.guilds:
            guild.forum = forum
        
            levels = []
            for b in guild.buildings:
                levels.append(str(b.buildLevel))
                
                if (guild.maxMembers-len(guild.members)) > 0:
                    dynamicSlots = str(guild.maxMembers-len(guild.members)) + " players"
                    if (guild.maxMembers-len(guild.members)) == 1:
                        dynamicSlots = str(guild.maxMembers-len(guild.members)) + " player"
                else:
                    dynamicSlots = "Full"
                        
                guildPosition = ""
                if guild.guildRank == 1:
                    guildPosition = "1st"
                elif guild.guildRank == 2:
                    guildPosition = "2nd"
                elif guild.guildRank == 3:
                    guildPosition = "3rd"
                else:
                    guildPosition = "%dth" % guild.guildRank
                
                
                
                words = {
                    "{%s-INITIALS}" % (guild.guildInitials):						guild.guildInitials,
                    "{%s-GUILD}" % (guild.guildInitials):						guild.guildName,
                    "{%s-RANK}" % (guild.guildInitials):						str(guild.guildRank),
                    "{%s-POSITION}" % (guild.guildInitials):						guildPosition,
                    "{%s-RANKTARGET}" % (guild.guildInitials):						str(guild.rankTarget),
                    "{%s-ENTRYLEVEL}" % (guild.guildInitials):						guild.entryLevel,
                    "{%s-BUILDINGS}" % (guild.guildInitials):						"/".join(levels),
                    "{%s-MEMBERS}" % (guild.guildInitials):						str(len(guild.members)),
                    "{%s-MAXMEMBERS}" % (guild.guildInitials):						str(guild.maxMembers),
                    "{%s-EMPTYSLOTS}" % (guild.guildInitials):						str(guild.maxMembers-len(guild.members)),
                    "{%s-DYNAMICSLOTS}" % (guild.guildInitials):					dynamicSlots,
                    "{%s-IOUSCORE}" % (guild.guildInitials):						guild.guildIOUScoreFF(),
                    "{%s-AVERAGELEVEL}" % (guild.guildInitials):					guild.averageLevelFF(),
                    "{%s-AVERAGEIOUSCORE}" % (guild.guildInitials):					guild.averageIOUScoreFF(),
                    "{%s-MEMBERSLIST}" % (guild.guildInitials):						guild.membersListFF(),
                    "{%s-BUILDINGSLIST}" % (guild.guildInitials):					guild.buildingsListFF("image"),
                    "{%s-BUILDINGSLIST-TEXT}" % (guild.guildInitials):				        guild.buildingsListFF("text"),
                    "{%s-MEMBERSCOUNTSTRING}" % (guild.guildInitials):				        guild.membersCountString(),
                    "{%s-LEVEL}" % (guild.guildInitials):				                str(guild.level),
                    "{%s-WISHINGWELL}" % (guild.guildInitials):				                str(guild.wishingwell),
                    "{%s-LEVELFF}" % (guild.guildInitials):				                guild.guildLevelFF(),
                    "{%s-WISHINGWELLFF}" % (guild.guildInitials):				        guild.wishingWellFF()
                }
                
                for w in words.keys():
                    self.words[w] = words[w]
                totalEmptySpots += guild.maxMembers-len(guild.members)
            self.words["{TOTALEMPTYSPOTS}"] = str(totalEmptySpots)
            self.words["{%s-INITIALS}" % (family.initials)] = family.initials
            self.words["{%s-NAME}" % (family.initials)] = family.name
            self.words["{%s-RANKINGSCHART}" % (family.initials)] = family.rankingsChart.getChartUrl()
            
    def parseText(self, text):
        """
        Translate special words to datas
        
        @param string				the template text
        @return string				the translated text
        """
        
        for word in self.words:
            text = text.replace(word, self.words[word])
            
        return text
